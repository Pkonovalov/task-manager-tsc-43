package ru.pkonovalov.tm;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.bootstrap.Bootstrap;
import ru.pkonovalov.tm.endpoint.Session;
import ru.pkonovalov.tm.endpoint.SessionEndpoint;

public abstract class AbstractEndpointTest {

    @NotNull
    protected static final String ADMIN_USER_NAME = "admin";
    @NotNull
    protected static final String ADMIN_USER_PASSWORD = "admin";
    @NotNull
    protected static final Bootstrap BOOTSTRAP = new Bootstrap();
    @NotNull
    protected static final SessionEndpoint SESSION_ENDPOINT = BOOTSTRAP.getSessionEndpoint();
    @NotNull
    protected static final String TEST_USER_NAME = "test";
    @NotNull
    protected static final String TEST_USER_PASSWORD = "test";
    @NotNull
    protected static Session SESSION = SESSION_ENDPOINT.openSession(TEST_USER_NAME, TEST_USER_PASSWORD);

}

package ru.pkonovalov.tm.api.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.endpoint.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;

public interface ISessionEndpoint {

    @WebMethod
    boolean closeSession(
            @WebParam(name = "session", partName = "session") @NotNull Session session
    );

    @WebMethod
    Session openSession(
            @WebParam(name = "login", partName = "login") @NotNull String login,
            @WebParam(name = "password", partName = "password") @NotNull String password
    );

}

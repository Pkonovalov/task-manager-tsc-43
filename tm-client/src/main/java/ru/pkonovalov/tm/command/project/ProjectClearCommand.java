package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectClearCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Delete all projects";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-clear";
    }

    @Override
    public void execute() {
        System.out.println("[PROJECT CLEAR]");
        endpointLocator.getProjectEndpoint().clearProject(endpointLocator.getSession());
    }

}

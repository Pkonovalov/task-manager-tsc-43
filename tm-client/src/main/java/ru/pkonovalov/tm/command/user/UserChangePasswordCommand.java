package ru.pkonovalov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.util.TerminalUtil;

public final class UserChangePasswordCommand extends AbstractUserCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Change user password";
    }

    @NotNull
    @Override
    public String commandName() {
        return "user-change-password";
    }

    @Override
    public void execute() {
        System.out.println("[CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        final String password = TerminalUtil.nextLine();
        endpointLocator.getUserEndpoint().updateUserPassword(endpointLocator.getSession(), password);
    }

}

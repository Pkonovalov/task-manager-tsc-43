package ru.pkonovalov.tm.command.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.endpoint.Status;
import ru.pkonovalov.tm.exception.entity.ProjectNotFoundException;
import ru.pkonovalov.tm.util.TerminalUtil;

import java.util.Arrays;

public final class ProjectByNameSetStatusCommand extends AbstractProjectCommand {

    @Nullable
    @Override
    public String commandArg() {
        return null;
    }

    @NotNull
    @Override
    public String commandDescription() {
        return "Set project status by name";
    }

    @NotNull
    @Override
    public String commandName() {
        return "project-set-status-by-name";
    }

    @Override
    public void execute() {
        System.out.println("[SET PROJECT STATUS]");
        System.out.println("ENTER NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        if (!endpointLocator.getProjectEndpoint().existsProjectByName(endpointLocator.getSession(), name))
            throw new ProjectNotFoundException();
        System.out.println("ENTER STATUS:");
        System.out.println(Arrays.toString(Status.values()));
        endpointLocator.getProjectEndpoint().setProjectStatusByName(endpointLocator.getSession(), name, Status.valueOf(TerminalUtil.nextLine()));
    }

}

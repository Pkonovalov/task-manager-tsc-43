package ru.pkonovalov.tm.endpoint;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlType;


/**
 * &lt;p&gt;Java class for bindTaskById complex type.
 * <p>
 * &lt;p&gt;The following schema fragment specifies the expected content contained within this class.
 * <p>
 * &lt;pre&gt;
 * &amp;lt;complexType name="bindTaskById"&amp;gt;
 * &amp;lt;complexContent&amp;gt;
 * &amp;lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType"&amp;gt;
 * &amp;lt;sequence&amp;gt;
 * &amp;lt;element name="session" type="{http://endpoint.tm.pkonovalov.ru/}session" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="projectId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 * &amp;lt;element name="taskId" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/&amp;gt;
 * &amp;lt;/sequence&amp;gt;
 * &amp;lt;/restriction&amp;gt;
 * &amp;lt;/complexContent&amp;gt;
 * &amp;lt;/complexType&amp;gt;
 * &lt;/pre&gt;
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "bindTaskById", propOrder = {
        "session",
        "projectId",
        "taskId"
})
public class BindTaskById {

    protected String projectId;
    protected Session session;
    protected String taskId;

    /**
     * Gets the value of the projectId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getProjectId() {
        return projectId;
    }

    /**
     * Sets the value of the projectId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setProjectId(String value) {
        this.projectId = value;
    }

    /**
     * Gets the value of the session property.
     *
     * @return possible object is
     * {@link Session }
     */
    public Session getSession() {
        return session;
    }

    /**
     * Sets the value of the session property.
     *
     * @param value allowed object is
     *              {@link Session }
     */
    public void setSession(Session value) {
        this.session = value;
    }

    /**
     * Gets the value of the taskId property.
     *
     * @return possible object is
     * {@link String }
     */
    public String getTaskId() {
        return taskId;
    }

    /**
     * Sets the value of the taskId property.
     *
     * @param value allowed object is
     *              {@link String }
     */
    public void setTaskId(String value) {
        this.taskId = value;
    }

}

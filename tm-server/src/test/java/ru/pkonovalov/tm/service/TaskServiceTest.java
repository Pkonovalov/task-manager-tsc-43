package ru.pkonovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.service.dto.ITaskService;
import ru.pkonovalov.tm.enumerated.Sort;
import ru.pkonovalov.tm.enumerated.Status;
import ru.pkonovalov.tm.model.Task;

import java.util.ArrayList;
import java.util.List;

public class TaskServiceTest extends AbstractTest {

    @NotNull
    protected static final String TEST_TASK_NAME = "Task test name";

    @NotNull
    protected static final String TEST_TASK_NAME_TWO = "Z Task test name";

    @NotNull
    private static final ITaskService TASK_SERVICE = BOOTSTRAP.getTaskService();

    @Nullable
    private static Task TEST_TASK;

    @NotNull
    private static String TEST_TASK_ID;

    @Test
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, TASK_SERVICE.findAll().size());
    }

    @Test
    public void clear() {
        TASK_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(TASK_SERVICE.findAll(TEST_USER_ID).isEmpty());
        TASK_SERVICE.clear();
        Assert.assertTrue(TASK_SERVICE.findAll().isEmpty());
    }

    @Test
    public void create() {
        Assert.assertNotNull(TEST_TASK);
        Assert.assertNotNull(TEST_TASK.getId());
        Assert.assertNotNull(TEST_TASK.getName());
        Assert.assertEquals(TEST_TASK_NAME, TEST_TASK.getName());
        Assert.assertNotNull(TASK_SERVICE.findAll(TEST_USER_ID));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(TASK_SERVICE.existsById(TEST_USER_ID, TEST_TASK_ID));
    }

    @Test
    public void existsByName() {
        Assert.assertTrue(TASK_SERVICE.existsByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    public void findByIndex() {
        Assert.assertEquals(TEST_TASK, TASK_SERVICE.findOneByIndex(TEST_USER_ID, 1));
        Assert.assertEquals(TEST_TASK_ID, TASK_SERVICE.getIdByIndex(TEST_USER_ID, 1));
    }

    @Test
    public void findByName() {
        Assert.assertEquals(TEST_TASK, TASK_SERVICE.findOneByName(TEST_USER_ID, TEST_TASK_NAME));
    }

    @Test
    public void finishById() {
        TASK_SERVICE.finishTaskById(TEST_USER_ID, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertEquals(Status.COMPLETE, TEST_TASK.getStatus());
    }

    @After
    public void finishTest() {
        BOOTSTRAP.getUserRepository().clear();
        TASK_SERVICE.clear();
        BOOTSTRAP.getProjectService().clear();
    }

    private void initTwo() {
        TASK_SERVICE.clear(TEST_USER_ID);
        @NotNull final Task task1 = new Task(TEST_TASK_NAME);
        task1.setUserId(TEST_USER_ID);
        task1.setStatus(Status.IN_PROGRESS);
        @NotNull final Task task2 = new Task(TEST_TASK_NAME_TWO);
        task2.setUserId(TEST_USER_ID);
        task2.setStatus(Status.COMPLETE);
        @NotNull final List<Task> list = new ArrayList<>();
        list.add(task1);
        list.add(task2);
        TASK_SERVICE.addAll(list);
    }

    @Test
    public void remove() {
        Assert.assertNotNull(TEST_TASK);
        TASK_SERVICE.remove(TEST_TASK);
        Assert.assertNull(TASK_SERVICE.findById(TEST_TASK.getId()));
        Assert.assertTrue(TASK_SERVICE.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    public void removeByIndex() {
        initTwo();
        TASK_SERVICE.removeOneByIndex(TEST_USER_ID, 1);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_SERVICE.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    public void removeByName() {
        initTwo();
        TASK_SERVICE.removeOneByName(TEST_USER_ID, TEST_TASK_NAME);
        Assert.assertEquals(TEST_TASK_NAME_TWO, TASK_SERVICE.findAll(TEST_USER_ID).get(0).getName());
    }

    @Test
    public void sortName() {
        initTwo();
        @NotNull final List<Task> list = TASK_SERVICE.findAll(TEST_USER_ID, Sort.NAME.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Test
    public void sortStarted() {
        initTwo();
        @NotNull final List<Task> list = TASK_SERVICE.findAll(TEST_USER_ID, Sort.STARTED.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Test
    public void sortStatus() {
        initTwo();
        @NotNull final List<Task> list = TASK_SERVICE.findAll(TEST_USER_ID, Sort.STATUS.getComparator());
        Assert.assertEquals(TEST_TASK_NAME, list.get(0).getName());
    }

    @Test
    public void startById() {
        TASK_SERVICE.startTaskById(TEST_USER_ID, TEST_TASK_ID);
        Assert.assertNotNull(TEST_TASK);
        Assert.assertEquals(Status.IN_PROGRESS, TEST_TASK.getStatus());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final Task task = new Task();
        TEST_TASK_ID = task.getId();
        task.setName(TEST_TASK_NAME);
        task.setUserId(TEST_USER_ID);
        TASK_SERVICE.add(task);
        TEST_TASK = TASK_SERVICE.findById(TEST_USER_ID, task.getId());
    }

}

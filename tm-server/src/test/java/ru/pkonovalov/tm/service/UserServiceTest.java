package ru.pkonovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.service.dto.IUserService;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.util.HashUtil;

public class UserServiceTest extends AbstractTest {

    @NotNull
    private static final String TEST_PASSWORD = "test-password";
    @NotNull
    private static final IUserService USER_SERVICE = BOOTSTRAP.getUserService();
    @NotNull
    private static UserDTO TEST_USER;

    @Test
    public void existByEmail() {
        Assert.assertTrue(USER_SERVICE.existsByEmail(TEST_USER_EMAIL));
    }

    @Test
    public void findByLogin() {
        Assert.assertEquals(TEST_USER, USER_SERVICE.findByLogin(TEST_USER_NAME));
    }

    @After
    public void finishTest() {
        SQL_SESSION.getMapper(IUserRepository.class).clear();
    }

    @Test
    public void removeByLogin() {
        USER_SERVICE.removeByLogin(TEST_USER_NAME);
        Assert.assertTrue(USER_SERVICE.findAll().isEmpty());
    }

    @Test
    public void setPassword() {
        USER_SERVICE.setPassword(TEST_USER_ID, TEST_PASSWORD);
        Assert.assertEquals(HashUtil.salt(BOOTSTRAP.getPropertyService(), TEST_PASSWORD), TEST_USER.getPasswordHash());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
    }

}

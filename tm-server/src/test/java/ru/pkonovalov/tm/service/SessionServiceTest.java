package ru.pkonovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.service.dto.ISessionService;
import ru.pkonovalov.tm.dto.SessionDTO;

public class SessionServiceTest extends AbstractTest {

    @NotNull
    private static final ISessionService SESSION_SERVICE = BOOTSTRAP.getSessionService();

    @Nullable
    private static SessionDTO TEST_SESSION;

    @NotNull
    private static String TEST_SESSION_ID;

    @Test
    public void checkPassword() {
        Assert.assertTrue(SESSION_SERVICE.checkDataAccess(TEST_USER_NAME, TEST_USER_PASSWORD));
    }

    @Test
    public void clear() {
        SESSION_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(SESSION_SERVICE.findAll(TEST_USER_ID).isEmpty());
        SESSION_SERVICE.clear();
        Assert.assertTrue(SESSION_SERVICE.findAll().isEmpty());
    }

    @After
    public void finishTest() {
        SQL_SESSION.getMapper(IUserRepository.class).clear();
        SESSION_SERVICE.clear();
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final SessionDTO session = new SessionDTO();
        TEST_SESSION_ID = session.getId();
        session.setUserId(TEST_USER_ID);
        SESSION_SERVICE.add(session);
        TEST_SESSION = SESSION_SERVICE.findById(TEST_SESSION_ID);
    }

}

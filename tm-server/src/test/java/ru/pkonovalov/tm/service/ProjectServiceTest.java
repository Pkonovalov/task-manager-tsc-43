package ru.pkonovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import ru.pkonovalov.tm.AbstractTest;
import ru.pkonovalov.tm.api.service.dto.IProjectService;
import ru.pkonovalov.tm.dto.ProjectDTO;
import ru.pkonovalov.tm.enumerated.Sort;
import ru.pkonovalov.tm.enumerated.Status;

import java.util.ArrayList;
import java.util.List;

public class ProjectServiceTest extends AbstractTest {

    @NotNull
    protected static final String TEST_PROJECT_NAME = "ProjectDTO test name";

    @NotNull
    protected static final String TEST_PROJECT_NAME_TWO = "Z ProjectDTO test name";

    @NotNull
    private static final IProjectService PROJECT_SERVICE = BOOTSTRAP.getProjectService();

    @Nullable
    private static ProjectDTO TEST_PROJECT;

    @NotNull
    private static String TEST_PROJECT_ID;

    @Test
    public void addAll() {
        initTwo();
        Assert.assertEquals(2, PROJECT_SERVICE.findAll().size());
    }

    @Test
    public void clear() {
        PROJECT_SERVICE.clear(TEST_USER_ID);
        Assert.assertTrue(PROJECT_SERVICE.findAll(TEST_USER_ID).isEmpty());
        PROJECT_SERVICE.clear();
        Assert.assertTrue(PROJECT_SERVICE.findAll().isEmpty());
    }

    @Test
    public void create() {
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertNotNull(TEST_PROJECT.getId());
        Assert.assertNotNull(TEST_PROJECT.getName());
        Assert.assertEquals(TEST_PROJECT_NAME, TEST_PROJECT.getName());
        Assert.assertNotNull(PROJECT_SERVICE.findAll(TEST_USER_ID));
    }

    @Test
    public void existsById() {
        Assert.assertTrue(PROJECT_SERVICE.existsById(TEST_USER_ID, TEST_PROJECT_ID));
    }

    @Test
    public void existsByName() {
        Assert.assertTrue(PROJECT_SERVICE.existsByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    public void findByName() {
        Assert.assertEquals(TEST_PROJECT, PROJECT_SERVICE.findOneByName(TEST_USER_ID, TEST_PROJECT_NAME));
    }

    @Test
    public void finishById() {
        PROJECT_SERVICE.finishProjectById(TEST_USER_ID, TEST_PROJECT_ID);
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertEquals(Status.COMPLETE, TEST_PROJECT.getStatus());
    }

    @After
    public void finishTest() {
        SQL_SESSION.getMapper(IUserRepository.class).clear();
        PROJECT_SERVICE.clear();
        BOOTSTRAP.getProjectService().clear();
    }

    private void initTwo() {
        PROJECT_SERVICE.clear(TEST_USER_ID);
        @NotNull final ProjectDTO project1 = new ProjectDTO(TEST_PROJECT_NAME);
        project1.setUserId(TEST_USER_ID);
        project1.setStatus(Status.IN_PROGRESS);
        @NotNull final ProjectDTO project2 = new ProjectDTO(TEST_PROJECT_NAME_TWO);
        project2.setUserId(TEST_USER_ID);
        project2.setStatus(Status.COMPLETE);
        @NotNull final List<ProjectDTO> list = new ArrayList<>();
        list.add(project1);
        list.add(project2);
        PROJECT_SERVICE.addAll(list);
    }

    @Test
    public void remove() {
        Assert.assertNotNull(TEST_PROJECT);
        PROJECT_SERVICE.remove(TEST_PROJECT);
        Assert.assertNull(PROJECT_SERVICE.findById(TEST_PROJECT.getId()));
        Assert.assertTrue(PROJECT_SERVICE.findAll(TEST_USER_ID).isEmpty());
    }

    @Test
    public void sortName() {
        initTwo();
        @NotNull final List<ProjectDTO> list = PROJECT_SERVICE.findAll(TEST_USER_ID, Sort.NAME.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Test
    public void sortStarted() {
        initTwo();
        @NotNull final List<ProjectDTO> list = PROJECT_SERVICE.findAll(TEST_USER_ID, Sort.STARTED.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Test
    public void sortStatus() {
        initTwo();
        @NotNull final List<ProjectDTO> list = PROJECT_SERVICE.findAll(TEST_USER_ID, Sort.STATUS.getComparator());
        Assert.assertEquals(TEST_PROJECT_NAME, list.get(0).getName());
    }

    @Test
    public void startById() {
        PROJECT_SERVICE.startProjectById(TEST_USER_ID, TEST_PROJECT_ID);
        Assert.assertNotNull(TEST_PROJECT);
        Assert.assertEquals(Status.IN_PROGRESS, TEST_PROJECT.getStatus());
    }

    @Before
    public void startTest() {
        TEST_USER = BOOTSTRAP.getAuthService().registry(TEST_USER_NAME, TEST_USER_PASSWORD, TEST_USER_EMAIL);
        TEST_USER_ID = TEST_USER.getId();
        @NotNull final ProjectDTO project = new ProjectDTO();
        TEST_PROJECT_ID = project.getId();
        project.setName(TEST_PROJECT_NAME);
        project.setUserId(TEST_USER_ID);
        PROJECT_SERVICE.add(project);
        TEST_PROJECT = PROJECT_SERVICE.findById(TEST_USER_ID, project.getId());
    }

}

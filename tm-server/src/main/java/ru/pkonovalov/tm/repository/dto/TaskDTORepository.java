package ru.pkonovalov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.ITaskDTORepository;
import ru.pkonovalov.tm.dto.TaskDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskDTORepository extends AbstractBusinessDTORepository<TaskDTO> implements ITaskDTORepository {

    public TaskDTORepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void bindTaskPyProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final TaskDTO task = findById(userId, taskId);
        if (task == null) return;
        task.setProjectId(projectId);
        update(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE TaskDTO where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE TaskDTO").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO WHERE userId = :userId AND projectId = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<TaskDTO> findAll(@Nullable final String userId) {
        return em.createQuery("FROM TaskDTO WHERE userId = :userId", TaskDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<TaskDTO> findAll() {
        return em.createQuery("FROM TaskDTO", TaskDTO.class).getResultList();
    }

    @Nullable
    @Override
    public List<TaskDTO> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("FROM TaskDTO WHERE userId = :userId AND projectId = :projectId", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM TaskDTO WHERE userId = :userId AND id = :id", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public TaskDTO findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM TaskDTO WHERE id = :id", TaskDTO.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public TaskDTO findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM TaskDTO WHERE userId = :userId AND name = :name", TaskDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Override
    public void removeAllBinded(@Nullable final String userId) {
        em.createQuery("DELETE TaskDTO WHERE userId = :userId AND projectId IS NOT NULL").executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        em.createQuery("DELETE TaskDTO WHERE userId = :userId AND projectId = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE TaskDTO WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO WHERE userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM TaskDTO", Long.class).setMaxResults(1).getSingleResult();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String id) {
        @Nullable final TaskDTO task = findById(userId, id);
        if (task == null) return;
        task.setProjectId(null);
        update(task);
    }

}

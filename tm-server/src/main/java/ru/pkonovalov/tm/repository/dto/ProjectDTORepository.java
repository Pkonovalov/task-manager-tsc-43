package ru.pkonovalov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.IProjectDTORepository;
import ru.pkonovalov.tm.dto.ProjectDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectDTORepository extends AbstractBusinessDTORepository<ProjectDTO> implements IProjectDTORepository {

    public ProjectDTORepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE ProjectDTO where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE ProjectDTO").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectDTO WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectDTO WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectDTO WHERE userId = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<ProjectDTO> findAll(@Nullable final String userId) {
        return em.createQuery("FROM ProjectDTO WHERE userId = :userId", ProjectDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<ProjectDTO> findAll() {
        return em.createQuery("FROM ProjectDTO", ProjectDTO.class).getResultList();
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM ProjectDTO WHERE userId = :userId AND id = :id", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public ProjectDTO findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM ProjectDTO WHERE id = :id", ProjectDTO.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public ProjectDTO findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM ProjectDTO WHERE userId = :userId AND name = :name", ProjectDTO.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public String getIdByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT id FROM ProjectDTO WHERE userId = :userId AND name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE FROM ProjectDTO WHERE userId = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name).executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM ProjectDTO WHERE userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM ProjectDTO", Long.class).setMaxResults(1).getSingleResult();
    }

}

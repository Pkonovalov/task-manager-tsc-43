package ru.pkonovalov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.IUserDTORepository;
import ru.pkonovalov.tm.dto.UserDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class UserDTORepository extends AbstractDTORepository<UserDTO> implements IUserDTORepository {

    public UserDTORepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void clear() {
        em.createQuery("DELETE UserDTO").executeUpdate();
    }

    @Override
    public boolean existsByEmail(@Nullable final String email) {
        return em.createQuery("SELECT COUNT(*) FROM UserDTO WHERE email = :email", Long.class)
                .setParameter("email", email)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM UserDTO WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByLogin(@Nullable final String login) {
        return em.createQuery("SELECT COUNT(*) FROM UserDTO WHERE login = :login", Long.class)
                .setParameter("login", login)
                .getSingleResult() > 0;
    }

    @NotNull
    @Override
    public List<UserDTO> findAll() {
        return em.createQuery("FROM UserDTO", UserDTO.class).getResultList();
    }

    @Nullable
    @Override
    public UserDTO findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM UserDTO WHERE id = :id", UserDTO.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public UserDTO findByLogin(@Nullable final String login) {
        return getEntity(em.createQuery("FROM UserDTO WHERE login = :login", UserDTO.class)
                .setParameter("login", login)
                .setMaxResults(1));
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        em.createQuery("DELETE UserDTO WHERE login = :login")
                .setParameter("login", login).executeUpdate();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM UserDTO", Long.class).setMaxResults(1).getSingleResult();
    }

}

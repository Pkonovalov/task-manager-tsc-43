package ru.pkonovalov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.model.ITaskRepository;
import ru.pkonovalov.tm.model.Task;

import javax.persistence.EntityManager;
import java.util.List;

public class TaskRepository extends AbstractBusinessRepository<Task> implements ITaskRepository {

    public TaskRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void bindTaskPyProjectId(@Nullable final String userId, @Nullable final String projectId, @Nullable final String taskId) {
        @Nullable final Task task = findById(userId, taskId);
        if (task == null) return;
        @Nullable final Project project = new ProjectRepository(em).findById(projectId);
        task.setProject(project);
        update(task);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE Task where user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE Task").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Task WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId AND project.id = :projectId", Long.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<Task> findAll(@Nullable final String userId) {
        return em.createQuery("FROM Task WHERE user.id = :userId", Task.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Task> findAll() {
        return em.createQuery("FROM Task", Task.class).getResultList();
    }

    @Nullable
    @Override
    public List<Task> findAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        return em.createQuery("FROM Task WHERE user.id = :userId AND project.id = :projectId", Task.class)
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .getResultList();
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM Task WHERE user.id = :userId AND id = :id", Task.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Task findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM Task WHERE id = :id", Task.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Task findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM Task WHERE user.id = :userId AND name = :name", Task.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Override
    public void removeAllBinded(@Nullable final String userId) {
        em.createQuery("DELETE Task WHERE user.id = :userId AND project.id IS NOT NULL").executeUpdate();
    }

    @Override
    public void removeAllByProjectId(@Nullable final String userId, @Nullable final String projectId) {
        em.createQuery("DELETE Task WHERE user.id = :userId AND project.id = :projectId")
                .setParameter("userId", userId)
                .setParameter("projectId", projectId)
                .executeUpdate();
    }

    @Override
    public void removeOneByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE Task WHERE user.id = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name)
                .executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM Task WHERE user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM Task", Long.class).setMaxResults(1).getSingleResult();
    }

    @Override
    public void unbindTaskFromProject(@Nullable final String userId, @Nullable final String id) {
        @Nullable final Task task = findById(userId, id);
        if (task == null) return;
        task.setProject(null);
        update(task);
    }

}

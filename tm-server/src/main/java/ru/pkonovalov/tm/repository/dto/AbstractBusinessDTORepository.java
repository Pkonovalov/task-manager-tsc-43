package ru.pkonovalov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.IAbstractBusinessDTORepository;
import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;

import javax.persistence.EntityManager;
import java.util.Objects;

public abstract class AbstractBusinessDTORepository<E extends AbstractBusinessEntityDTO> extends AbstractDTORepository<E> implements IAbstractBusinessDTORepository<E> {

    public AbstractBusinessDTORepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Nullable
    public E findByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        return Objects.requireNonNull(findAll(userId)).get(index);
    }

    @Nullable
    @Override
    public String getIdByIndex(@Nullable final String userId, @Nullable final Integer index) {
        if (index == null) return null;
        return Objects.requireNonNull(findAll(userId)).get(index).getId();
    }

    @Override
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        remove(findById(userId, id));
    }

    @Override
    public void removeByIndex(@Nullable final String userId, @Nullable final Integer index) {
        remove(findByIndex(userId, index));
    }
}
package ru.pkonovalov.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.ISessionDTORepository;
import ru.pkonovalov.tm.dto.SessionDTO;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionDTORepository extends AbstractBusinessDTORepository<SessionDTO> implements ISessionDTORepository {

    public SessionDTORepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE SessionDTO where userId = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE SessionDTO").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM SessionDTO WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM SessionDTO WHERE userId = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<SessionDTO> findAll(@Nullable final String userId) {
        return em.createQuery("FROM SessionDTO WHERE userId = :userId", SessionDTO.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<SessionDTO> findAll() {
        return em.createQuery("FROM SessionDTO", SessionDTO.class).getResultList();
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM SessionDTO WHERE userId = :userId AND id = :id", SessionDTO.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public SessionDTO findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM SessionDTO WHERE id = :id", SessionDTO.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM SessionDTO WHERE userId = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM SessionDTO", Long.class).setMaxResults(1).getSingleResult();
    }

}

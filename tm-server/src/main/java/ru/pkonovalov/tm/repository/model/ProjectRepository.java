package ru.pkonovalov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.model.IProjectRepository;
import ru.pkonovalov.tm.model.Project;

import javax.persistence.EntityManager;
import java.util.List;

public class ProjectRepository extends AbstractBusinessRepository<Project> implements IProjectRepository {

    public ProjectRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE Project where user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE Project").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Project WHERE user.id = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Project WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT COUNT(*) FROM Project WHERE user.id = :userId AND name = :name", Long.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<Project> findAll(@Nullable final String userId) {
        return em.createQuery("FROM Project WHERE user.id = :userId", Project.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Project> findAll() {
        return em.createQuery("FROM Project", Project.class).getResultList();
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM Project WHERE user.id = :userId AND id = :id", Project.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Project findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM Project WHERE id = :id", Project.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Project findByName(@Nullable final String userId, @Nullable final String name) {
        return getEntity(em.createQuery("FROM Project WHERE user.id = :userId AND name = :name", Project.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public String getIdByName(@Nullable final String userId, @Nullable final String name) {
        return em.createQuery("SELECT id FROM Project WHERE user.id = :userId AND name = :name", String.class)
                .setParameter("userId", userId)
                .setParameter("name", name)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public void removeByName(@Nullable final String userId, @Nullable final String name) {
        em.createQuery("DELETE FROM Project WHERE user.id = :userId AND name = :name")
                .setParameter("userId", userId)
                .setParameter("name", name).executeUpdate();
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM Project WHERE user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM Project", Long.class).setMaxResults(1).getSingleResult();
    }

}

package ru.pkonovalov.tm.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.model.ISessionRepository;
import ru.pkonovalov.tm.model.Session;

import javax.persistence.EntityManager;
import java.util.List;

public class SessionRepository extends AbstractBusinessRepository<Session> implements ISessionRepository {

    public SessionRepository(@NotNull final EntityManager em) {
        super(em);
    }

    @Override
    public void clear(@Nullable final String userId) {
        em.createQuery("DELETE Session where user.id = :userId")
                .setParameter("userId", userId).executeUpdate();
    }

    @Override
    public void clear() {
        em.createQuery("DELETE Session").executeUpdate();
    }

    @Override
    public boolean existsById(@Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Session WHERE id = :id", Long.class)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Override
    public boolean existsById(@Nullable final String userId, @Nullable final String id) {
        return em.createQuery("SELECT COUNT(*) FROM Session WHERE user.id = :userId AND id = :id", Long.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .getSingleResult() > 0;
    }

    @Nullable
    @Override
    public List<Session> findAll(@Nullable final String userId) {
        return em.createQuery("FROM Session WHERE user.id = :userId", Session.class)
                .setParameter("userId", userId)
                .getResultList();
    }

    @NotNull
    @Override
    public List<Session> findAll() {
        return em.createQuery("FROM Session", Session.class).getResultList();
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String userId, @Nullable final String id) {
        return getEntity(em.createQuery("FROM Session WHERE user.id = :userId AND id = :id", Session.class)
                .setParameter("userId", userId)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @Nullable
    @Override
    public Session findById(@Nullable final String id) {
        return getEntity(em.createQuery("FROM Session WHERE id = :id", Session.class)
                .setParameter("id", id)
                .setMaxResults(1));
    }

    @NotNull
    @Override
    public Long size(@Nullable final String userId) {
        return em.createQuery("SELECT COUNT(*) FROM Session WHERE user.id = :userId", Long.class)
                .setParameter("userId", userId)
                .setMaxResults(1)
                .getSingleResult();
    }

    @Override
    public Long size() {
        return em.createQuery("SELECT COUNT(*) FROM Session", Long.class).setMaxResults(1).getSingleResult();
    }

}

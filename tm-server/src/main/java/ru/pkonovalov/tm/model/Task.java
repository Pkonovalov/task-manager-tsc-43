package ru.pkonovalov.tm.model;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.enumerated.Status;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "app_task")
@NoArgsConstructor
public class Task extends AbstractEntity {

    @Column
    @Nullable
    private Date created = new Date();

    @Column
    @Nullable
    private Date dateFinish;

    @Column
    @Nullable
    private Date dateStart;

    @Column
    @Nullable
    private String description;

    @Column
    @Nullable
    private String name;

    @ManyToOne
    private Project project;

    @Nullable
    @Enumerated(EnumType.STRING)
    private Status status = Status.NOT_STARTED;

    public void setStatus(@Nullable final Status status) {
        if (status == null) return;
        this.status = status;
        switch (status) {
            case IN_PROGRESS:
                this.setDateStart(new Date());
                break;
            case COMPLETE:
                this.setDateFinish(new Date());
            default:
                break;
        }
    }

}

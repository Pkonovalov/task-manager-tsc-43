package ru.pkonovalov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.enumerated.Role;

import java.util.List;

public interface IUserDTOService extends IAbstractDTOService<UserDTO> {

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @Nullable String email);

    @NotNull
    UserDTO create(@Nullable String login, @Nullable String password, @NotNull Role role);

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    UserDTO findByLogin(@Nullable String login);

    void lockUserByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

    void setPassword(@NotNull String userId, @Nullable String password);

    void unlockUserByLogin(@Nullable String login);

    void updateUser(@NotNull String userId, @Nullable String firstName, @Nullable String lastName, @Nullable String middleName);

}


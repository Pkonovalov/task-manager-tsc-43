package ru.pkonovalov.tm.api.service;

import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;

import javax.persistence.EntityManager;
import java.sql.Connection;

public interface IConnectionService {

    @NotNull EntityManager getEntityManager();

}

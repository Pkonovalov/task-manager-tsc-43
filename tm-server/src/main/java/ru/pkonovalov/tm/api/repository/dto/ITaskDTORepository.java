package ru.pkonovalov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.TaskDTO;

import java.util.List;

public interface ITaskDTORepository extends IAbstractBusinessDTORepository<TaskDTO> {

    void bindTaskPyProjectId(@Nullable String userId, @Nullable String projectId, @Nullable String taskId);

    boolean existsByName(@Nullable String userId, @Nullable String name);

    boolean existsByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    List<TaskDTO> findAllByProjectId(@Nullable String userId, @Nullable String projectId);

    @Nullable
    TaskDTO findByName(@Nullable String userId, @Nullable String name);

    void removeAllBinded(@Nullable String userId);

    void removeAllByProjectId(@Nullable String userId, @Nullable String projectId);

    void removeOneByName(@Nullable String userId, @Nullable String name);

    void unbindTaskFromProject(@Nullable String userId, @Nullable String id);

}

package ru.pkonovalov.tm.api.repository.model;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.model.User;

public interface IUserRepository extends IAbstractRepository<User> {

    boolean existsByEmail(@Nullable String email);

    boolean existsByLogin(@Nullable String login);

    @Nullable
    User findByLogin(@Nullable String login);

    void removeByLogin(@Nullable String login);

}

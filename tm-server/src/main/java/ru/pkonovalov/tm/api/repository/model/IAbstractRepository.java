package ru.pkonovalov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.model.AbstractEntity;

import java.util.List;

public interface IAbstractRepository<E extends AbstractEntity> {

    void add(@Nullable E entity);

    void add(@Nullable List<E> entities);

    void clear();

    boolean existsById(@Nullable String id);

    @Nullable
    List<E> findAll();

    @Nullable
    E findById(@NotNull String id);

    void remove(@Nullable E entity);

    void removeById(@Nullable String id);

    Long size();

    void update(@Nullable E entity);

}

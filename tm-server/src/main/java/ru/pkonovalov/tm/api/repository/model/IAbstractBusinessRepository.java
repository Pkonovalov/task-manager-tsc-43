package ru.pkonovalov.tm.api.repository.model;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.model.AbstractBusinessEntity;
import ru.pkonovalov.tm.model.AbstractEntity;

import java.util.List;

public interface IAbstractBusinessRepository<E extends AbstractBusinessEntity> extends IAbstractRepository<E> {

    void clear(@Nullable String userId);

    boolean existsById(@Nullable String userId, @Nullable String id);

    @Nullable
    List<E> findAll(@Nullable String userId);


    @Nullable
    E findById(@Nullable String userId, @Nullable String id);

    @Nullable
    E findByIndex(@Nullable String userId, @Nullable Integer index);

    @Nullable
    String getIdByIndex(@Nullable String userId, @Nullable Integer index);

    void removeById(@Nullable String userId, @Nullable String id);

    void removeByIndex(@Nullable String userId, @Nullable Integer index);

    @NotNull Long size(@Nullable String userId);

}

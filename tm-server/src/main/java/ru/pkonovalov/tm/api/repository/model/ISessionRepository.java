package ru.pkonovalov.tm.api.repository.model;

import ru.pkonovalov.tm.model.Session;

public interface ISessionRepository extends IAbstractBusinessRepository<Session> {

}
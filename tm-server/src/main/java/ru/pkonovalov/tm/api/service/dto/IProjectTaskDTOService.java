package ru.pkonovalov.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.TaskDTO;
import java.util.List;

public interface IProjectTaskDTOService {

    void bindTaskByProjectId(@NotNull String userId, @Nullable String projectId, @Nullable String taskId);

    void clearTasks(@NotNull String userId);

    @Nullable
    List<TaskDTO> findAllTaskByProjectId(@NotNull String userId, @Nullable String projectId);

    void removeProjectById(@NotNull String userId, @Nullable String projectId);

    void removeProjectByIndex(@NotNull String userId, @NotNull Integer projectIndex);

    void removeProjectByName(@NotNull String userId, @Nullable String projectName);

    void unbindTaskFromProject(@NotNull String userId, @Nullable String taskId);

}

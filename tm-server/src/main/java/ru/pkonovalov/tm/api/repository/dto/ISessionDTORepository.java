package ru.pkonovalov.tm.api.repository.dto;

import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.ProjectDTO;
import ru.pkonovalov.tm.dto.SessionDTO;

public interface ISessionDTORepository extends IAbstractBusinessDTORepository<SessionDTO> {

}
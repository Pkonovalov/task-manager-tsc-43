package ru.pkonovalov.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.enumerated.Role;

public interface IAuthService {

    void checkRoles(@Nullable Role... roles);

    @Nullable
    UserDTO getUser();

    @NotNull
    String getUserId();

    boolean isAuth();

    void login(@Nullable String login, @Nullable String password);

    void logout();

    @NotNull
    UserDTO registry(@Nullable String login, @Nullable String password, @Nullable String email);

}

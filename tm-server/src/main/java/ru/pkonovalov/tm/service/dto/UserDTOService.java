package ru.pkonovalov.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.dto.IUserDTORepository;
import ru.pkonovalov.tm.api.service.dto.IUserDTOService;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.exception.empty.EmptyEmailException;
import ru.pkonovalov.tm.exception.empty.EmptyIdException;
import ru.pkonovalov.tm.exception.empty.EmptyLoginException;
import ru.pkonovalov.tm.exception.empty.EmptyPasswordException;
import ru.pkonovalov.tm.exception.entity.UserNotFoundException;
import ru.pkonovalov.tm.exception.user.EmailExistsException;
import ru.pkonovalov.tm.exception.user.LoginExistsException;
import ru.pkonovalov.tm.repository.dto.UserDTORepository;
import ru.pkonovalov.tm.util.HashUtil;

import javax.persistence.EntityManager;
import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class UserDTOService implements IUserDTOService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public UserDTOService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
        this.connectionService = connectionService;
    }

    @SneakyThrows
    public UserDTO add(@Nullable final UserDTO user) {
        if (user == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.add(user);
            em.getTransaction().commit();
            return user;
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void add(@Nullable List<UserDTO> list) {
        if (list == null) throw new UserNotFoundException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.add(list);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void clear() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.clear();
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @NotNull
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (isEmpty(email)) throw new EmptyEmailException();
        if (existsByLogin(login)) throw new LoginExistsException();
        if (existsByEmail(email)) throw new EmailExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setEmail(email);
        add(user);
        return user;
    }

    @NotNull
    @SneakyThrows
    public UserDTO create(@Nullable final String login, @Nullable final String password, @NotNull final Role role) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        if (existsByLogin(login)) throw new LoginExistsException();
        @NotNull final UserDTO user = new UserDTO();
        user.setLogin(login);
        user.setPasswordHash(HashUtil.salt(propertyService, password));
        user.setRole(role);
        add(user);
        return user;
    }

    @SneakyThrows
    public boolean existsByEmail(@Nullable final String email) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.existsByEmail(email);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public boolean existsById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.existsById(id);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public boolean existsByLogin(@Nullable final String login) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.existsByLogin(login);
        } finally {
            em.close();
        }
    }

    @NotNull
    @SneakyThrows
    public List<UserDTO> findAll() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.findAll();
        } finally {
            em.close();
        }
    }

    @Nullable
    @SneakyThrows
    public UserDTO findById(@NotNull final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.findById(id);
        } finally {
            em.close();
        }
    }

    @Nullable
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            return repository.findByLogin(login);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void lockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            @Nullable final UserDTO user = repository.findByLogin(login);
            if (user == null) throw new UserNotFoundException();
            user.setLocked(true);
            update(user);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void remove(@Nullable final UserDTO user) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.remove(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void removeById(@Nullable final String id) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.removeById(id);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @Override
    public void removeByLogin(@Nullable final String login) {
        remove(findByLogin(login));
    }

    @SneakyThrows
    public void setPassword(@NotNull final String userId, @Nullable final String password) {
        if (isEmpty(userId)) throw new EmptyIdException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            @Nullable final UserDTO user = repository.findById(userId);
            if (user == null) throw new UserNotFoundException();
            user.setPasswordHash(HashUtil.salt(propertyService, password));
            update(user);
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public Long size() {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository userRepository = new UserDTORepository(em);
            return userRepository.size();
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void unlockUserByLogin(@Nullable final String login) {
        if (isEmpty(login)) throw new EmptyLoginException();
        @Nullable final UserDTO user = findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        user.setLocked(false);
        update(user);
    }

    @SneakyThrows
    public void update(@Nullable final UserDTO user) {
        @NotNull final EntityManager em = connectionService.getEntityManager();
        try {
            em.getTransaction().begin();
            @NotNull final IUserDTORepository repository = new UserDTORepository(em);
            repository.update(user);
            em.getTransaction().commit();
        } catch (@NotNull final Exception e) {
            em.getTransaction().rollback();
            throw e;
        } finally {
            em.close();
        }
    }

    @SneakyThrows
    public void updateUser(
            @NotNull final String userId,
            @Nullable final String firstName,
            @Nullable final String lastName,
            @Nullable final String middleName
    ) {
        @Nullable final UserDTO user = findById(userId);
        if (user == null) throw new UserNotFoundException();
        user.setFirstName(firstName);
        user.setMiddleName(middleName);
        user.setLastName(lastName);
        update(user);
    }

}

package ru.pkonovalov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.service.IAuthService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.api.service.dto.IUserDTOService;
import ru.pkonovalov.tm.dto.UserDTO;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.exception.empty.EmptyLoginException;
import ru.pkonovalov.tm.exception.empty.EmptyPasswordException;
import ru.pkonovalov.tm.exception.entity.UserNotFoundException;
import ru.pkonovalov.tm.exception.user.AccessDeniedException;
import ru.pkonovalov.tm.exception.user.NotLoggedInException;
import ru.pkonovalov.tm.util.HashUtil;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public class AuthService implements IAuthService {

    @NotNull
    private final IPropertyService propertyService;
    @NotNull
    private final IUserDTOService userService;
    @Nullable
    private String userId;

    public AuthService(
            @NotNull final IUserDTOService userService,
            @NotNull final IPropertyService propertyService
    ) {
        this.userService = userService;
        this.propertyService = propertyService;
    }

    @Override
    public void checkRoles(@Nullable final Role... roles) {
        if (roles == null || roles.length == 0) return;
        @Nullable final UserDTO user = getUser();
        if (user == null) throw new UserNotFoundException();
        @Nullable final Role role = user.getRole();
        for (@Nullable final Role item : roles) {
            if (item == null) continue;
            if (item.equals(role)) return;
        }
        throw new AccessDeniedException();
    }

    @Nullable
    @Override
    public UserDTO getUser() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        @Nullable final String userId = getUserId();
        return userService.findById(userId);
    }

    @NotNull
    @Override
    public String getUserId() {
        if (userId == null) throw new NotLoggedInException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login)) throw new EmptyLoginException();
        if (isEmpty(password)) throw new EmptyPasswordException();
        @Nullable final String hash = HashUtil.salt(propertyService, password);
        if (hash == null) throw new AccessDeniedException();
        @Nullable final UserDTO user = userService.findByLogin(login);
        if (user == null) throw new UserNotFoundException();
        if (!hash.equals(user.getPasswordHash()) || user.isLocked()) throw new AccessDeniedException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        if (isEmpty(userId)) throw new NotLoggedInException();
        userId = null;
    }

    @NotNull
    @Override
    public UserDTO registry(@Nullable final String login, @Nullable final String password, @Nullable final String email) {
        return userService.create(login, password, email);
    }

}

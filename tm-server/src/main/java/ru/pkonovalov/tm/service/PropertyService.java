package ru.pkonovalov.tm.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.service.IPropertyService;

import java.io.InputStream;
import java.util.Properties;

public class PropertyService implements IPropertyService {

    @NotNull
    private static final String APPLICATION_VERSION_DEFAULT = "";
    @NotNull
    private static final String APPLICATION_VERSION_KEY = "application.version";
    @NotNull
    private static final String BACKUP_INTERVAL_DEFAULT = "5";
    @NotNull
    private static final String BACKUP_INTERVAL_KEY = "backup.interval";
    private static final int BACKUP_INTERVAL_MIN = 1;
    @NotNull
    private static final String FILE_NAME = "application.properties";
    @NotNull
    private static final String JDBC_DIALECT_DEFAULT = "org.hibernate.dialect.MySQL5InnoDBDialect";
    @NotNull
    private static final String JDBC_DIALECT_KEY = "jdbc.dialect";
    @NotNull
    private static final String JDBC_DRIVER_DEFAULT = "com.mysql.jdbc.Driver";
    @NotNull
    private static final String JDBC_DRIVER_KEY = "jdbc.driver";
    @NotNull
    private static final String JDBC_HBM2DDL_DEFAULT = "update";
    @NotNull
    private static final String JDBC_HBM2DDL_KEY = "jdbc.hbm2ddl";
    @NotNull
    private static final String JDBC_PASSWORD_DEFAULT = "root";
    @NotNull
    private static final String JDBC_PASSWORD_KEY = "jdbc.password";
    @NotNull
    private static final String JDBC_SHOW_SQL_DEFAULT = "true";
    @NotNull
    private static final String JDBC_SHOW_SQL_KEY = "jdbc.showsql";
    @NotNull
    private static final String JDBC_URL_DEFAULT = "jdbc:mysql://localhost:3306/task-manager?zeroDateTimeBehavior=convertToNull";
    @NotNull
    private static final String JDBC_URL_KEY = "jdbc.url";
    @NotNull
    private static final String JDBC_USERNAME_DEFAULT = "root";
    @NotNull
    private static final String JDBC_USERNAME_KEY = "jdbc.username";
    @NotNull
    private static final String PASSWORD_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String PASSWORD_ITERATION_KEY = "password.iteration";
    @NotNull
    private static final String PASSWORD_SECRET_DEFAULT = "";
    @NotNull
    private static final String PASSWORD_SECRET_KEY = "password.secret";
    @NotNull
    private static final String SCANNER_INTERVAL_DEFAULT = "5";
    @NotNull
    private static final String SCANNER_INTERVAL_KEY = "scanner.interval";
    private static final int SCANNER_INTERVAL_MIN = 1;
    @NotNull
    private static final String SERVER_HOST_DEFAULT = "localhost";
    @NotNull
    private static final String SERVER_HOST_KEY = "server.host";
    @NotNull
    private static final String SERVER_PORT_DEFAULT = "8080";
    @NotNull
    private static final String SERVER_PORT_KEY = "server.port";
    @NotNull
    private static final String SESSION_ITERATION_DEFAULT = "1";
    @NotNull
    private static final String SESSION_ITERATION_KEY = "session.iteration";
    @NotNull
    private static final String SESSION_SECRET_DEFAULT = "";
    @NotNull
    private static final String SESSION_SECRET_KEY = "session.secret";
    @NotNull
    private final Properties properties = new Properties();

    @SneakyThrows
    public PropertyService() {
        @Nullable final InputStream inputStream = ClassLoader.getSystemResourceAsStream(FILE_NAME);
        if (inputStream == null) return;
        properties.load(inputStream);
        inputStream.close();
    }

    @NotNull
    @Override
    public String getApplicationVersion() {
        @Nullable final String systemProperty = System.getProperty(APPLICATION_VERSION_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(APPLICATION_VERSION_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(APPLICATION_VERSION_KEY, APPLICATION_VERSION_DEFAULT);
    }

    @Override
    public int getBackupInterval() {
        @Nullable final String systemProperty = System.getProperty(BACKUP_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), BACKUP_INTERVAL_MIN);
        @Nullable final String environmentProperty = System.getenv(BACKUP_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), BACKUP_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(BACKUP_INTERVAL_KEY, BACKUP_INTERVAL_DEFAULT));
        return Math.max(interval, BACKUP_INTERVAL_MIN);
    }

    @Override
    public @NotNull String getJdbcDialect() {
        @Nullable final String systemProperty = System.getProperty(JDBC_DIALECT_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_DIALECT_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_DIALECT_KEY, JDBC_DIALECT_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcDriver() {
        @Nullable final String systemProperty = System.getProperty(JDBC_DRIVER_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_DRIVER_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_DRIVER_KEY, JDBC_DRIVER_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcHBM2DDL() {
        @Nullable final String systemProperty = System.getProperty(JDBC_HBM2DDL_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_HBM2DDL_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_HBM2DDL_KEY, JDBC_HBM2DDL_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcPassword() {
        @Nullable final String systemProperty = System.getProperty(JDBC_PASSWORD_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_PASSWORD_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_PASSWORD_KEY, JDBC_PASSWORD_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcShowSql() {
        @Nullable final String systemProperty = System.getProperty(JDBC_SHOW_SQL_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_SHOW_SQL_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_SHOW_SQL_KEY, JDBC_SHOW_SQL_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUrl() {
        @Nullable final String systemProperty = System.getProperty(JDBC_URL_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_URL_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_URL_KEY, JDBC_URL_DEFAULT);
    }

    @Override
    public @NotNull String getJdbcUsername() {
        @Nullable final String systemProperty = System.getProperty(JDBC_USERNAME_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(JDBC_USERNAME_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(JDBC_USERNAME_KEY, JDBC_USERNAME_DEFAULT);
    }

    @NotNull
    @Override
    public Integer getPasswordIteration() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(PASSWORD_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(PASSWORD_ITERATION_KEY, PASSWORD_ITERATION_DEFAULT)
        );
    }

    @NotNull
    @Override
    public String getPasswordSecret() {
        @Nullable final String systemProperty = System.getProperty(PASSWORD_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(PASSWORD_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(PASSWORD_SECRET_KEY, PASSWORD_SECRET_DEFAULT);
    }

    @Override
    public int getScannerInterval() {
        @Nullable final String systemProperty = System.getProperty(SCANNER_INTERVAL_KEY);
        if (systemProperty != null) return Math.max(Integer.parseInt(systemProperty), SCANNER_INTERVAL_MIN);
        @Nullable final String environmentProperty = System.getenv(SCANNER_INTERVAL_KEY);
        if (environmentProperty != null) return Math.max(Integer.parseInt(environmentProperty), SCANNER_INTERVAL_MIN);
        final int interval = Integer.parseInt(properties.getProperty(SCANNER_INTERVAL_KEY, SCANNER_INTERVAL_DEFAULT));
        return Math.max(interval, SCANNER_INTERVAL_MIN);
    }

    @Override
    public @NotNull String getServerHost() {
        @Nullable final String systemProperty = System.getProperty(SERVER_HOST_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(SERVER_HOST_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(SERVER_HOST_KEY, SERVER_HOST_DEFAULT);
    }

    @Override
    public int getServerPort() {
        @Nullable final String systemProperty = System.getProperty(SERVER_PORT_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(SERVER_PORT_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(properties.getProperty(SERVER_PORT_KEY, SERVER_PORT_DEFAULT));
    }

    @Override
    public int getSessionCycle() {
        @Nullable final String systemProperty = System.getProperty(SESSION_ITERATION_KEY);
        if (systemProperty != null) return Integer.parseInt(systemProperty);
        @Nullable final String environmentProperty = System.getenv(SESSION_ITERATION_KEY);
        if (environmentProperty != null) return Integer.parseInt(environmentProperty);
        return Integer.parseInt(
                properties.getProperty(SESSION_ITERATION_KEY, SESSION_ITERATION_DEFAULT)
        );
    }

    @Override
    public @NotNull String getSessionSalt() {
        @Nullable final String systemProperty = System.getProperty(SESSION_SECRET_KEY);
        if (systemProperty != null) return systemProperty;
        @Nullable final String environmentProperty = System.getenv(SESSION_SECRET_KEY);
        if (environmentProperty != null) return environmentProperty;
        return properties.getProperty(SESSION_SECRET_KEY, SESSION_SECRET_DEFAULT);
    }

}


package ru.pkonovalov.tm.service.dto;

import org.jetbrains.annotations.NotNull;
import ru.pkonovalov.tm.api.service.dto.IAbstractDTOService;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.dto.AbstractBusinessEntityDTO;


public abstract class AbstractDTOService<E extends AbstractBusinessEntityDTO> implements IAbstractDTOService<E> {

    @NotNull
    private IConnectionService connectionService;

    public AbstractDTOService(@NotNull final IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

}

package ru.pkonovalov.tm.service.model;

import lombok.SneakyThrows;
import org.apache.ibatis.session.SqlSession;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.repository.model.ISessionRepository;
import ru.pkonovalov.tm.api.service.IConnectionService;
import ru.pkonovalov.tm.api.service.IPropertyService;
import ru.pkonovalov.tm.api.service.model.ISessionService;
import ru.pkonovalov.tm.dto.SessionDTO;
import ru.pkonovalov.tm.enumerated.Role;
import ru.pkonovalov.tm.exception.entity.EntityNotFoundException;
import ru.pkonovalov.tm.exception.user.AccessDeniedException;
import ru.pkonovalov.tm.model.Session;
import ru.pkonovalov.tm.model.User;
import ru.pkonovalov.tm.service.model.AbstractService;
import ru.pkonovalov.tm.util.HashUtil;
import ru.pkonovalov.tm.util.SignatureUtil;

import java.util.List;

import static ru.pkonovalov.tm.util.ValidationUtil.isEmpty;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IConnectionService connectionService;
    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final IConnectionService connectionService, @NotNull final IPropertyService propertyService) {
        this.connectionService = connectionService;
        this.propertyService = propertyService;
    }

    @NotNull
    @SneakyThrows
    public Session add(@Nullable final Session session) {
        if (session == null) throw new EntityNotFoundException();
        @Nullable final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.add(session);
            return session;
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean checkDataAccess(@Nullable final String login, @Nullable final String password) {
        if (isEmpty(login) || isEmpty(password)) return false;
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) return false;
            if (user.isLocked()) return false;
            @Nullable final String passwordHash = HashUtil.salt(propertyService, password);
            if (isEmpty(passwordHash)) return false;
            return passwordHash.equals(user.getPasswordHash());
        }
    }

    @Override
    @SneakyThrows
    public void clear(@NotNull final String userId) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clear(userId);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.clearAll();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    public void close(final @NotNull SessionDTO session) {
        validate(session);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session);
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void closeAll(@NotNull final Session session) {
        validate(session);
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeByUserId(session.getUserId());
            sqlSession.commit();
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean existsById(@NotNull final String userId, @NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.existsById(userId, id);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAllOfUser(userId);
        }
    }

    @NotNull
    @Override
    @SneakyThrows
    public List<Session> findAll() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findAll();
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String userId, @NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findById(userId, id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session findById(@NotNull final String id) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findByIdAll(id);
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Session> getListSession(@NotNull final Session session) {
        validate(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.findByUserId(session.getUserId());
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public User getUser(final @NotNull SessionDTO session) {
        @NotNull final String userId = getUserId(session);
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            return userRepository.findById(userId);
        }
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        validate(session);
        return session.getUserId();
    }

    @Override
    public boolean isValid(@NotNull final Session session) {
        try {
            validate(session);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    @Nullable
    @Override
    @SneakyThrows
    public Session open(@Nullable final String login, @Nullable final String password) {
        final boolean check = checkDataAccess(login, password);
        if (!check || isEmpty(login)) return null;
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findByLogin(login);
            if (user == null) return null;
            @NotNull final Session session = new Session();
            session.setUserId(user.getId());
            session.setTimestamp(System.currentTimeMillis());
            add(session);
            sqlSession.commit();
            return sign(session);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@Nullable final Session session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.remove(session);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(@NotNull final String userId, @Nullable final String id) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.removeById(userId, id);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Nullable
    @Override
    public Session sign(@Nullable final Session session) {
        if (session == null) return null;
        session.setSignature(null);
        @Nullable final String salt = propertyService.getSessionSalt();
        final int cycle = propertyService.getSessionCycle();
        @Nullable final String signature = SignatureUtil.sign(session, salt, cycle);
        session.setSignature(signature);
        update(session);
        return session;
    }

    @Override
    @SneakyThrows
    public int size() {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.sizeAll();
        }
    }

    @Override
    @SneakyThrows
    public int size(@NotNull final String userId) {
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            return sessionRepository.size(userId);
        }
    }

    @SneakyThrows
    public void update(@Nullable final Session session) {
        @NotNull final SqlSession sqlSession = connectionService.getSqlSession();
        try {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            sessionRepository.update(session);
        } catch (@NotNull final Exception e) {
            sqlSession.rollback();
            throw e;
        } finally {
            sqlSession.commit();
            sqlSession.close();
        }
    }

    @Override
    @SneakyThrows
    public void validate(final @NotNull SessionDTO session, @Nullable final Role role) {
        if (role == null) throw new AccessDeniedException();
        validate(session);
        @Nullable final String userId = session.getUserId();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final IUserRepository userRepository = sqlSession.getMapper(IUserRepository.class);
            @Nullable final User user = userRepository.findById(userId);
            if (user == null) throw new AccessDeniedException();
            if (user.getRole() == null) throw new AccessDeniedException();
            if (!role.equals(user.getRole())) throw new AccessDeniedException();
        }
    }

    @Override
    @SneakyThrows
    public void validate(final @NotNull SessionDTO session) {
        if (session == null) throw new AccessDeniedException();
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        @Nullable final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final Session sessionSign = sign(temp);
        if (sessionSign == null) throw new AccessDeniedException();
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        try (@NotNull final SqlSession sqlSession = connectionService.getSqlSession()) {
            @NotNull final ISessionRepository sessionRepository = sqlSession.getMapper(ISessionRepository.class);
            if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
        }
    }
}

package ru.pkonovalov.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.bootstrap.Bootstrap;
import ru.pkonovalov.tm.util.SystemUtil;

public class Application {

    public static void main(@Nullable final String[] args) {
        System.out.println(SystemUtil.getPID());
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}

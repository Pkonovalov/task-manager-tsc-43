package ru.pkonovalov.tm.comparator;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pkonovalov.tm.api.entity.IHasCreated;

import java.util.Comparator;

@NoArgsConstructor
public final class ComparatorByCreated implements Comparator<IHasCreated> {

    private static @NotNull
    final ComparatorByCreated INSTANCE = new ComparatorByCreated();


    public static @NotNull ComparatorByCreated getInstance() {
        return INSTANCE;
    }

    @Override
    public int compare(@Nullable final IHasCreated o1, @Nullable final IHasCreated o2) {
        if (o1 == null || o2 == null) return 0;
        if (o1.getCreated() == null) return -1;
        if (o2.getCreated() == null) return 1;
        return o1.getCreated().compareTo(o2.getCreated());
    }

}
